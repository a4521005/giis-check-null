// https://www.benq.eu/en-eu/sitemap.xml

//https://www.benq.eu/en-eu/projector/accessory/wireless-full-hd-kit-wdp01/specifications.html有問題
const puppeteer = require('puppeteer');
const request = require("request-promise");
const cheerio = require("cheerio");
const expect = require('chai').expect;

const eneuSpecUrlAll=[]
const eneuPass=[]
const eneuFail=[]


const eneuSpecUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            if(url.indexOf("specifications")!==-1){
                eneuSpecUrlAll.push(url)
            }
    })
    return eneuSpecUrlAll;
    } catch (error) {
      console.log(error);
    }
  };


describe('B2C Product Spec',async()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('B2C ENEU - Check All Spec URL',async function(){
        const returnedData = await eneuSpecUrl();
        //console.log(returnedData)
        console.log('Total of ENEU Spec URL :',returnedData.length)
        //console.log("type of Data:",typeof returnedData)
        await page.goto("https://www.benq.eu/en-eu/index.html")
        await page.waitForSelector('#btn_close')
        await page.click('#btn_close')
        for(let i =0;i<returnedData.length;i++){
            const specUrlCheck = returnedData[i]
            await page.goto(specUrlCheck)
            //body > div.productspecification > section > div > div
            //div.com_container.v_align.v_align_middle
            const innerHtml = await page.$eval('body', element => element.innerHTML);
            if(innerHtml.indexOf("data_row")!==-1){
                eneuPass.push(specUrlCheck)
                //console.log(`${specUrlCheck} is pass`)
            }else{
                console.log(`Fail URL: ${specUrlCheck}`)
                eneuFail.push(specUrlCheck)
            }
        }
        console.log("Total of B2C ENEU Pass URL:",eneuPass.length)
        //console.log("B2C ENEU Pass URL:",pass)
        console.log("Total of B2C ENEU Fail Url:",eneuFail.length)
        console.log("B2C ENEU Fail Url:",eneuFail)
        expect(eneuFail.length).to.equal(0)
    })
})
