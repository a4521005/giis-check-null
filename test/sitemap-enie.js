// https://www.benq.eu/en-ie/sitemap.xml
// npm run test:sitemap-enie
const puppeteer = require('puppeteer');
const request = require("request-promise");
const cheerio = require("cheerio");
const expect = require('chai').expect;

const cicGA="?utm_source=autotest&utm_medium=CIC"
const enieSpecUrlAll=[]
const eniePass=[]
const enieFail=[]


const enieSpecUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            // if(url.indexOf("specifications")!==-1 && url.indexOf("wireless-full-hd-kit-wdp01")==-1){
            //     eneuSpecUrlAll.push(url)
            // }
            //取得所有specifications的URL, 並且排除所有software & accessary
            if(url.indexOf("specifications")!==-1 && url.indexOf("accessory")==-1 && url.indexOf("paper-color-sync")==-1 && url.indexOf("palette-master-element")==-1 && url.indexOf("display-pilot")==-1){
                enieSpecUrlAll.push(url)
            }
    })
    return enieSpecUrlAll;
    } catch (error) {
      console.log(error);
    }
  };


describe('B2C Product Spec',async()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('B2C ENIE - Check All Spec URL',async function(){
        const returnedData = await enieSpecUrl();
        //console.log(returnedData)
        console.log('Total of ENIE Spec URL :',returnedData.length)
        //console.log("type of Data:",typeof returnedData)
        await page.goto("https://www.benq.eu/en-ie/index.html"+cicGA)
        await page.waitForSelector('#btn_close')
        await page.click('#btn_close')
        for(let i =0;i<returnedData.length;i++){
            const specUrlCheck = returnedData[i]
            await page.goto(specUrlCheck+cicGA)

            //section.table_two_color > div:nth-child(1) > div:nth-child(1)
            const innerHtml = await page.$eval('body', element => element.innerHTML);
            if(innerHtml.indexOf("data_row")!==-1){
                eniePass.push(specUrlCheck)
                //console.log(`${specUrlCheck} is pass`)
            }else{
                console.log(`Fail URL: ${specUrlCheck}`)
                enieFail.push(specUrlCheck)
            }
        }
        console.log("Total of B2C ENIE Pass URL:",eniePass.length)
        //console.log("B2C ENEU Pass URL:",pass)
        console.log("Total of B2C ENIE Fail Url:",enieFail.length)
        console.log("B2C ENIE Fail Url:",enieFail)
        expect(enieFail.length).to.equal(0)
    })
})
