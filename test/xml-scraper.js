const { sitemapUrlScraper } = require("xml-sitemap-url-scraper");

let sitemapUrls = [
    "https://www.benq.com/en-us/sitemap.xml"
]

// Define how many compressed sitemaps we want to decompress and process at once (if any are found)
let concurrency = 5;

// Function's concurrency defaults to 1 if no param is provided
let urls = sitemapUrlScraper(sitemapUrls, concurrency);

urls.then(result => {
    console.log("Returned URLs: ", result)
})
.catch(err => {
    console.log(err);
})


//node ./test/xml-scraper.js