// https://www.benq.com/en-hk/sitemap.xml
//npm run test:sitemap-enhk
const puppeteer = require('puppeteer');
const request = require("request-promise");
const cheerio = require("cheerio");
const expect = require('chai').expect;

const enhkSpecUrlAll=[]
const enhkPass=[]
const enhkFail=[]

const enhkSpecUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            if(url.indexOf("specifications")!==-1){
                enhkSpecUrlAll.push(url)
            }
    })
    return enhkSpecUrlAll;
    } catch (error) {
      console.log(error);
    }
  };


describe('B2C Product Spec',async()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('B2C ENHK - Check All Spec URL',async function(){
        const returnedData = await enhkSpecUrl();
        //console.log(returnedData)
        console.log('Total of ENHK Spec URL :',returnedData.length)
        //console.log("type of Data:",typeof returnedData)
        for(let i =0;i<returnedData.length;i++){
            const specUrlCheck = returnedData[i]
            await page.goto(specUrlCheck)
            //div.com_container.v_align.v_align_middle
            const innerHtml = await page.$eval('body', element => element.innerHTML);
            if(innerHtml.indexOf("data_row")!==-1){
                enhkPass.push(specUrlCheck)
                //console.log(`${specUrlCheck} is pass`)
            }else{
                console.log(`Fail URL: ${specUrlCheck}`)
                enhkFail.push(specUrlCheck)
            }
        }
        console.log("Total of B2C ENHK Pass URL:",enhkPass.length)
        //console.log("B2C ENUS Pass URL:",pass)
        console.log("Total of B2C ENHK Fail Url:",enhkFail.length)
        console.log("B2C ENHK Fail Url:",enhkFail)
        expect(enhkFail.length).to.equal(0)
    })
})
