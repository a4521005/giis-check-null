// https://www.benq.com/en-us/sitemap.xml
const puppeteer = require('puppeteer');
const request = require("request-promise");
const cheerio = require("cheerio");
const forEach = require('mocha-each');
const expect = require('chai').expect;

const specUrl=[]
// const specUrlAll=[]
const enusPass=[]
const enusFail=[]


const enusSpecUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            if(url.indexOf("specifications")!==-1){
                
                // console.log(url)
                // const urlGet = {}
                // urlGet.urlSpec = url
                // specUrl.push(urlGet)
                specUrl.push(url)
                // specUrlAll.push(specUrl)
            }
    })
    return specUrl;
    // return specUrlAll;
    } catch (error) {
      console.log(error);
    }
  };
//   async function main(){
//     const returnedData = await enusSpecUrl();
//     console.log(returnedData.length);
//     return returnedData
//   };
//   main()


describe('B2C Product Spec',async()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            //executablePath:
            //"./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // const returnedData = await enusSpecUrl();
    // console.log(returnedData)
    // console.log(returnedData.length)
    // forEach(returnedData)
    // .it('ENUS test scenario', async(url) => {
    //     fetchData(url)
    //       .then(url => await page.goto(url))
    //       .then(url => await page.waitForSelector('div.data_row', {visible:true}))
    //       .then(done);
    //   });

    // returnedData.forEach(function(value, i) {
    //     it(`ENUS test scenario ${i}`, async function() {
    //         await page.goto(value)
    //         await page.waitForSelector('div.data_row', {visible:true})
    //         //assert.deepEqual(testValue, value);
    //     })
    //   })

    // //test case
    // it('B2C ENUS - Check All Spec URL',async function(){
    //     const returnedData = await enusSpecUrl();
    //     console.log(returnedData)
    //     console.log('Total of ENUS Spec URL :',returnedData.length)
    //     console.log("type of Data:",typeof returnedData)
    //     const specUrlCheck = returnedData[0]
    //     await page.goto(specUrlCheck)
    //     await page.waitForSelector('div.data_row', {visible:true})
    // })
    // it('B2C ENUS - Check All Spec URL',async function(){
    //     const returnedData = await enusSpecUrl();
    //     const specUrlCheck = returnedData[1]
    //     await page.goto(specUrlCheck)
    //     await page.waitForSelector('div.data_row', {visible:true})
    // })

    //loop
    // it('B2C ENUS - Check All Spec URL',async function(){
    //     const returnedData = await enusSpecUrl();
    //     console.log(returnedData)
    //     console.log('Total of ENUS Spec URL :',returnedData.length)
    //     console.log("type of Data:",typeof returnedData)
    //     for(let i =0;i<returnedData.length;i++){
    //         const specUrlCheck = returnedData[i]
    //         await page.goto(specUrlCheck)
    //         await page.waitForSelector('div.data_row', {visible:true})
    //     }
    // })
    it('B2C ENUS - Check All Spec URL',async function(){
        const returnedData = await enusSpecUrl();
        //console.log(returnedData)
        console.log('Total of ENUS Spec URL :',returnedData.length)
        //console.log("type of Data:",typeof returnedData)
        for(let i =0;i<returnedData.length;i++){
            const specUrlCheck = returnedData[i]
            await page.goto(specUrlCheck)
            const innerHtml = await page.$eval('div.com_container.v_align.v_align_middle', element => element.innerHTML);
            if(innerHtml.indexOf("data_row")!==-1){
                enusPass.push(specUrlCheck)
                //console.log(`${specUrlCheck} is pass`)
            }else{
                console.log(`Fail URL: ${specUrlCheck}`)
                enusFail.push(specUrlCheck)
            }
        }
        console.log("Total of B2C ENUS Pass URL:",enusPass.length)
        //console.log("B2C ENUS Pass URL:",pass)
        console.log("Total of B2C ENUS Fail Url:",enusFail.length)
        console.log("B2C ENUS Fail Url:",enusFail)
        expect(enusFail.length).to.equal(0)
    })

    // it('B2C ENUS - Check All Spec URL',async function(){
    //     const returnedData = await enusSpecUrl();
    //     console.log(returnedData)
    //     console.log('Total of ENUS Spec URL :',returnedData.length)
    //     console.log("type of Data:",typeof returnedData)
    //     for(let i =0;i<returnedData.length;i++){
    //         it(`Check ${returnedData[i]}`,async function(){
    //             const specUrlCheck = returnedData[i]
    //             await page.goto(specUrlCheck)
    //             await page.waitForSelector('div.data_row', {visible:true})
    //         })
            
    //     }
    // })
})

//https://mochajs.org/ 
//forEach