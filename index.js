const request = require("request-promise")
const cheerio = require("cheerio")

async function main(){
    const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
    const $ = cheerio.load(result)
    const specUrl = []
    $("url > loc").each((index,element)=>{
        let url = $(element).text()
        if(url.indexOf("specifications")!==-1){
            // console.log(url)
            specUrl.push(url)
        }
    })
    console.log(specUrl)
}
main()

//node index.js